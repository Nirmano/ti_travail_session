/**
*class Camera en Singleton
*/
public static class Camera 
{
  private static Camera instance;
  public static PVector location = new PVector(325/2,325/2);
  
  /**
  *creation de la camera si elle n'existe pas deja
  */
  public static Camera getInstance()
  {
    if(instance == null)
    {
      instance  = new Camera();
    }
    return instance;
  }
  
  /**
  *retourne la location de la camera
  */
  public static PVector getLocation() 
  {
    return location;
  }
  
  
  ///place la location de la camera
  
  public static void setLocation(float x, float y) 
  {
    location.x = x;
    location.y = y;
  }
}

/**
*source Nicolas Bourré
*reference https://refactoring.guru/design-patterns/singleton/java/example
*/
