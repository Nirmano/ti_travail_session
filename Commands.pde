/**
*Commande pour le deplacement du personnage
*/

/**
*Commande Haut
*/
class UpCommand implements ICommand
{
  public void execute()
  {
    /**
    *deplacement du personnage et de la camera
    */
    caracters.setMove(new PVector(0,-2));
    Camera.setLocation(Camera.getLocation().x,Math.max(Camera.getLocation().y - 2,Math.min(0, (map.getMapWidth() - carreLargeur) * tileSize)));
  }
}

/**
*Commande Bas
*/
class DownCommand implements ICommand
{
  public void execute()
  {
    /**
    *deplacement du personnage et de la camera
    */
    caracters.setMove(new PVector(0,2));
    Camera.setLocation(Camera.getLocation().x,Math.max(Camera.getLocation().y + 2,Math.min(0, (map.getMapWidth() - carreLargeur) * tileSize)));
  }
}

/**
*Commande Gauche
*/
class LeftCommand implements ICommand
{
  public void execute()
  {
    /**
    *deplacement du personnage et de la camera
    */
    caracters.setMove(new PVector(-2,0));
    Camera.setLocation(Math.max(Camera.getLocation().x - 2, Math.min(0, (map.getMapWidth() - carreLargeur) * tileSize)) ,Camera.getLocation().y);
  }
}

/**
*Commande Droite
*/
class RightCommand implements ICommand
{
  public void execute()
  {
    /**
    *deplacement du personnage et de la camera
    */
    caracters.setMove(new PVector(4,0));
    Camera.setLocation(Math.max(Camera.getLocation().x + 4, Math.min(0, (map.getMapWidth() - carreLargeur) * tileSize)) ,Camera.getLocation().y); 
  }
}
/**
* reference https://refactoring.guru/design-patterns/command/java/example
*/
