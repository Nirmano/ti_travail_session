class GraphicObject
{
  PVector location;
  PVector velocity;
  PVector acceleration;
  color fillColor;
  color strokeColor;
  color strokeWeight;

  void update(float deltaTime) { }

  void display(){ }
}
