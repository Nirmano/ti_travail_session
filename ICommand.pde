/**
*Interface Commande
*/
public interface ICommand
{
  public void execute();
}

/**
* reference https://refactoring.guru/design-patterns/command/java/example
*/
