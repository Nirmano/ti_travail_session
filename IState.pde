/**
///Interface État
*/
public interface IState
{
  void next(State etat, Mover m);
}
/**
*reference https://refactoring.guru/design-patterns/state/java/example
*/
