/**
*Classe Personnage
*/
public class Personnage extends GraphicObject
{
  PVector location;
  PVector Velocity;
  PVector acceleration;
  float heading;
  float size = 30;
  Boolean canshoot = true;  
  int dimension = 1600;
  
/**
*Création Personnage
*/
  public Personnage()
  {
    this.location = new PVector(width/2, height/2);
    this.Velocity = new PVector(0,0);
    this.heading = 0;
  }

/**
*Update Personnage
*/
  void update(float deltaTime)
  {
    checkEdges();
    this.location.add(this.Velocity);
  }

/**
*Affichage Personnage
*/
  void display()
  {
    pushMatrix();
    translate(this.location.x, this.location.y);
    fill(255);
    ellipse(0,0,size,size);
    popMatrix();    
  }

/**
*Mouvement Personnage
*/
  void setMove(PVector location)
  {
    this.Velocity =  location;
  }
  
/**
*verification des bordure
*/
  
  void checkEdges() 
  {
    if (location.x < 32*3) 
    {
     Velocity = new PVector(0,0);
    } 
    else if (location.x > width - (32*3)) 
    {
     Velocity = new PVector(0,0);
    }
    
    if (location.y < 32*3) 
    {
     Velocity = new PVector(0,0);
    } 
    else if (location.y > height - (32*3)) 
    {
     Velocity = new PVector(0,0);
    }
  } 
}
