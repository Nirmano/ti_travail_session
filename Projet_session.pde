int currentTime;
int previousTime;
int deltaTime;

/**
*Personnage
*/
Personnage caracters;

/**
*Camera
*/
Camera camera = Camera.getInstance();

/**
*Commande
*/
RemoteControl remotecontrole = new RemoteControl();

/**
*Ennemie
*/
ArrayList<Mover> flockblue;
ArrayList<Mover> flockpurple;
ArrayList<Mover> flockpink;
ArrayList<Mover> flockred;
ArrayList<Mover> flockyellow;
ArrayList<Mover> flockgreen;
ArrayList<Mover> flockturquoise;
ArrayList<Mover> flockgray;
int flockSize = 10;

/**
*Image
*/
PImage ennemis,ennemi;
int wEnnemi = 31*3;
int hEnnemi = 32*4;
int choseEnnemiX, choseEnnemiY;

/**
*Carte
*/
TileMap map;
int carreLargeur = 10;
int carreHauteur = 10;
int tileSize = 32;
Rectangle tileRectangle = new Rectangle();

void setup () {
  currentTime = millis();
  previousTime = millis();
  size(325,325);
  ennemis = loadImage("ennemi.png");
  caracters = new Personnage();
  
  /**
  *Creation carte
  */
   map = new TileMap();
  
  Tile.setTileSetTexture(loadImage("data/part1_tileset.png"));
  Tile.setSourceRectangle(tileRectangle);
 
  
  /**
  *creation Ennemie
  */
  separation(ennemis);
  choseEnnemiX = 0;
  choseEnnemiY = 0;
  
  flockblue = new ArrayList<Mover>();
  color x = color(0,0,255);
  createFlock(flockblue,x,ennemi);
  
  separation(ennemis);
  choseEnnemiX = 1;
  choseEnnemiY = 0;
  
  flockpurple = new ArrayList<Mover>();
  x = color(77,0,57);
  createFlock(flockpurple,x,ennemi);
  
  separation(ennemis);
  choseEnnemiX = 2;
  choseEnnemiY = 0;
  
  flockpink = new ArrayList<Mover>();
  x = color(255,0,255);
  createFlock(flockpink,x,ennemi);
  
  separation(ennemis);
  choseEnnemiX = 3;
  choseEnnemiY = 0;
  
  flockred = new ArrayList<Mover>();
  x = color(255,0,0);
  createFlock(flockred,x,ennemi);
  
  separation(ennemis);
  choseEnnemiX = 0;
  choseEnnemiY = 1;
  
  flockyellow = new ArrayList<Mover>();
  x = color(255,255,0);
  createFlock(flockyellow,x,ennemi);
  
  separation(ennemis);
  choseEnnemiX = 1;
  choseEnnemiY = 1;
  
  flockgreen = new ArrayList<Mover>();
  x = color(0,255,0);
  createFlock(flockgreen,x,ennemi);
  
  separation(ennemis);
  choseEnnemiX = 2;
  choseEnnemiY = 1;
  
  flockturquoise = new ArrayList<Mover>();
  x = color(0,255,255);
  createFlock(flockturquoise,x,ennemi);
  
  separation(ennemis);
  choseEnnemiX = 3;
  choseEnnemiY = 1;
  
  flockgray = new ArrayList<Mover>();
  x = color(115,115,115);
  createFlock(flockgray,x,ennemi);

}

void draw () {
  currentTime = millis();
  deltaTime = currentTime - previousTime;

  update(deltaTime);
  display();

  previousTime = currentTime;
}


/**
*update des ennemies
*/
void update(int delta) 
{
  updateFlock(flockblue,delta);
  updateFlock(flockpurple,delta);
  updateFlock(flockpink,delta);
  updateFlock(flockred,delta);
  updateFlock(flockyellow,delta);
  updateFlock(flockgreen,delta);
  updateFlock(flockturquoise,delta);
  updateFlock(flockgray,delta);
}


void display () 
{
  background(0);
/**
*Carte
*/

  PVector firstSquare = new PVector(camera.getLocation().x / (float)tileSize, camera.getLocation().y / (float)tileSize);
  
  int firstX = (int) firstSquare.x;
  int firstY = (int) firstSquare.y;
  
  /// Le petit offset entre le bord de la caméra et la dimension d'une tuile
  PVector squareOffset = new PVector(camera.getLocation().x % tileSize, camera.getLocation().y % tileSize);
    
  int offsetX = (int) squareOffset.x;
  int offsetY = (int) squareOffset.y;
  
  for (int y = 0; y < carreHauteur; y++) 
  {
    int positionY = (y * tileSize) - offsetY;
    for (int x = 0; x < carreLargeur; x++) 
    {
      /// Va chercher le rectangle de la tuile à afficher
      Rectangle srcRect = Tile.getSourceRectangle(map.getRow(y + firstY).getCell(x + firstX).getTileID());
      
      PImage currentTile = Tile.getTileSetTexture().get((int)srcRect.location.x, (int)srcRect.location.y, (int)srcRect.w, (int)srcRect.h);
  
      image(currentTile,(x * tileSize) - offsetX, positionY);
    }
  }
  /**
  *source Nicolas Bourré
  */
/**
*Personnage
*/
  caracters.display();
  caracters.update(deltaTime); 
  if(!keyPressed)
  {  
    caracters.setMove(new PVector(0,0));
  }

/**
*ennemie
*/
displayFlock(flockblue);
displayFlock(flockpurple);
displayFlock(flockpink);
displayFlock(flockred);
displayFlock(flockyellow);
displayFlock(flockgreen);
displayFlock(flockturquoise);
displayFlock(flockgray);
}

/**
*ACTION
*/

void keyReleased()
{
}


void keyPressed()
{
  remotecontrole.InputHandler();
}


/**
*Afficher Ennemie
*/

void displayFlock(ArrayList<Mover> flock)
{
    for (Mover m : flock) 
  {
    m.display();
  }
}

/**
*Mettre a jour Ennemie
*/

void updateFlock(ArrayList<Mover> flock,int delta)
{
  for (Mover m : flock) 
  {
    m.flock(flock);
    m.update(delta);
  }
}

/**
*Créer Ennemie
*/

void createFlock(ArrayList<Mover> flock, color Color, PImage ennemi)
{
  for (int i = 0; i < flockSize; i++) 
  {
    Mover m = new Mover(new PVector(((width/2)-(width/3)),((height/2)-(height/3))), new PVector(random (-2, 2), random(-2, 2)), Color, ennemi);
    flock.add(m);
  }
  flock.get(0).debug = true;
}

/**
*Séparation image
*/
void separation(PImage ennemis)
{
  ennemi = ennemis.get(wEnnemi*choseEnnemiX,hEnnemi*choseEnnemiY,wEnnemi,hEnnemi);
}
