import java.util.Map;
/**
*Contient les commandes
*/

public class RemoteControl
{
  private UpCommand upbutton;
  private DownCommand downbutton;
  private LeftCommand leftbutton;
  private RightCommand rightbutton;
  private HashMap<String, ICommand> hm;
  
  RemoteControl()
  {
    upbutton = new UpCommand();
    downbutton = new DownCommand();
    leftbutton = new LeftCommand();
    rightbutton = new RightCommand();
    hm  = new HashMap<String, ICommand>();
    
    hm.put("a",leftbutton);
    hm.put("s",downbutton);
    hm.put("d",rightbutton);
    hm.put("w",upbutton);
  }
  
  /**
  *Appel la bonne commande
  */
  void InputHandler()
  {
    String s = Character.toString(key);
    if(hm.containsKey(s))
    {
      hm.get(s).execute();
    }
  }  
}
/**
* reference https://refactoring.guru/design-patterns/command/java/example
*/
