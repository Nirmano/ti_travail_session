/**
*état des ennemies
*/
public class State
{
  private IState etat = new FirstState();
  
  public void nextState(Mover m)
  {
    etat.next(this,m);
  }
  
  IState getState()
  {
    return etat;
  }
  
  void setState(IState etat)
  {
    this.etat = etat;
  }
}
/**
*reference https://refactoring.guru/design-patterns/state/java/example
*/
