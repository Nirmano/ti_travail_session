public class TileMap 
{
  ArrayList<MapRow> rows = new ArrayList<MapRow>();
  int mapWidth = 50;
  int mapHeight = 50;
  
  public TileMap()
  {
    for (int y = 0; y < mapHeight; y++) 
    {
      MapRow currentRow = new MapRow();
      for (int x = 0; x < mapWidth; x++) 
      {
        currentRow.columns.add(new MapCell(0));
      }
      rows.add(currentRow);
    }
    generateTest();
  }

  public MapRow getRow(int rowIndex) 
  {
    return rows.get(rowIndex);
  }

  public String getMapInString() 
  {
    String result = "";
    for (int y = 0; y < mapHeight; y++) 
    {
      MapRow currentRow = rows.get(y);
      for (int x = 0; x < mapWidth; x++) 
      {
        result += nf(currentRow.getCell(x).getTileID(), 2, 0) + " ";
      }
      result += "\r\n";
    }    
    return result;
  }
  
  private void generateTest() 
  {
    /// Début de la création
    JSONObject json = loadJSONObject("map.json");
    JSONArray jsonArray = json.getJSONArray("layers");
    JSONObject jsonData = jsonArray.getJSONObject(0);
    JSONArray index = jsonData.getJSONArray("data");
    int tileIndex [] = index.getIntArray();
    
    for(int i = 0; i < json.getInt("height");i++)
    {
      for(int j = 0; j < json.getInt("width");j++)
      {
          rows.get(i).columns.get(j).setTileID(tileIndex[j*50+i]-1);
      }
    }
  }

  public int getMapWidth() 
  {
    return mapWidth;
  }

  public void setMapWidth(int mapWidth) 
  {
    this.mapWidth = mapWidth;
  }

  public int getMapHeight() 
  {
    return mapHeight;
  }

  public void setMapHeight(int mapHeight) 
  {
    this.mapHeight = mapHeight;
  }
}

/**
*source Nicolas Bourré
*/
