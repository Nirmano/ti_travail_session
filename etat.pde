/**
*premier etat
*/
public class FirstState implements IState
{
  @Override
  void next(State etat, Mover m)
  {
    m.setEtatMover(0);
    etat.setState(new SecondState());
  }
}

/**
*deuxieme etat
*/
public class SecondState implements IState
{
  @Override
  void next(State etat, Mover m)
  {
    m.setEtatMover(1);
    etat.setState(new ThridState());
  }
}

/**
*troisieme etat
*/
public class ThridState implements IState
{
  @Override
  void next(State etat, Mover m)
  {
    m.setEtatMover(2);
    etat.setState(new FirstState());
  }
}
/**
*reference https://refactoring.guru/design-patterns/state/java/example
*/
