 class Mover extends GraphicObject {
  float topSpeed = 5;
  float topSteer = 0.05;
  int dimension = 1600;
  float mass = 1;
  
  float theta = 0;
  int r = 10; /// Rayon du boid
  
  float radiusSeparation = 10 * r;
  float radiusAlignment = 20 * r;
  float radiusCohesion = 30 * r;

  float weightSeparation = 2;
  float weightAlignment = 1;
  float weightCohesion = 1;
  
  PVector steer;
  PVector sumAlignment;
  PVector sumCohesion;

  PVector zeroVector = new PVector(0, 0);
  
/**
*State
*/
State state = new State();

  boolean debug = false;
  int msgCount = 0;
  String debugMessage = "";
  
  PImage face,ennemi;
  int wFace = 31;
  int hFace = 32;
  int etatMover,dirFace;
  int x = 50;
  
/**
*Création Mover
*/
  Mover () {
    location = new PVector();
    velocity = new PVector();
    acceleration = new PVector();
  }
  
  Mover (PVector loc, PVector vel, color fcolor, PImage Imover) {
    this.location = loc;
    this.velocity = vel;
    this.acceleration = new PVector (0 , 0);
    this.fillColor = fcolor;
    this.ennemi = Imover;
    etatMover = 0;
    dirFace = 0;
  }
  
/**
*Vérification bordure ecran
*/  
  void checkEdges() {
    if (location.x < 0) {
    //  location.x = dimension - r;
     velocity.x = velocity.x * -1;
    } else if (location.x + r> dimension) {
    //  location.x = 0;
          velocity.x = velocity.x * -1;
    }
    
    if (location.y < 0) {
    //  location.y = dimension - r;
         velocity.y = velocity.y * -1;

    } else if (location.y + r> dimension) {
    //  location.y = 0;
         velocity.y = velocity.y * -1;
    }
  }
  
/**
*Création Flock
*/  
  void flock (ArrayList<Mover> boids) {
    PVector separation = separate(boids);
    PVector alignment = align(boids);
    PVector cohesion = cohesion(boids);
    
    separation.mult(weightSeparation);
    alignment.mult(weightAlignment);
    cohesion.mult(weightCohesion);

    applyForce(separation);
    applyForce(alignment);
    applyForce(cohesion);
  }
  
/**
*Update Flock
*/   
  void update(float deltaTime) {
    checkEdges();

    velocity.add (acceleration);
    velocity.limit(topSpeed);

    theta = velocity.heading() + radians(90);

    location.add (velocity);

    acceleration.mult (0);      
  }
  
/**
*Affichage Mover
*/  
  void display() {
    state.nextState(this);
    noStroke();
    fill (fillColor);
    
    pushMatrix();
      translate(location.x, location.y);
      displayImage(etatMover);
    popMatrix();
    
    if (debug || this.debug) {
      renderDebug();
    }
  }

  void setEtatMover(int i)
  {
    etatMover = i;
  }
  
/**
*Separation Flock
*/ 
  PVector separate (ArrayList<Mover> boids) {
    if (steer == null) {
      steer = new PVector(0, 0, 0);
    }
    else {
      steer.setMag(0);
    }
    
    int count = 0;
    
    for (Mover other : boids) {
      float d = PVector.dist(location, other.location);
      
      if (d > 0 && d < radiusSeparation) {
        PVector diff = PVector.sub(location, other.location);
        
        diff.normalize();
        diff.div(d);
        
        steer.add(diff);
        
        count++;
      }
    }
    
    if (count > 0) {
      steer.div(count);
    }
    
    if (steer.mag() > 0) {
      steer.setMag(topSpeed);
      steer.sub(velocity);
      steer.limit(topSteer);
    }
    
    return steer;
  }

/**
*Alignement Flock
*/
  PVector align (ArrayList<Mover> boids) {

    if (sumAlignment == null) {
      sumAlignment = new PVector();      
    } else {
      sumAlignment.mult(0);
    }

    int count = 0;

    for (Mover other : boids) {
      float d = PVector.dist(this.location, other.location);

      if (d > 0 && d < radiusAlignment) {
        sumAlignment.add(other.velocity);
        count++;
      }
    }

    if (count > 0) {
      sumAlignment.div((float)count);
      sumAlignment.setMag(topSpeed);

      PVector steer = PVector.sub(sumAlignment, this.velocity);
      steer.limit(topSteer);

      return steer;
    } else {
      return zeroVector;
    }
  }
/**
* Méthode qui calcule et applique une force de braquage vers une cible
* STEER = CIBLE moins VITESSE
*/
  PVector seek (PVector target) {
    /// Vecteur différentiel vers la cible
    PVector desired = PVector.sub (target, this.location);
    
    /// VITESSE MAXIMALE VERS LA CIBLE
    desired.setMag(topSpeed);
    
    /// Braquage
    PVector steer = PVector.sub (desired, velocity);
    steer.limit(topSteer);
    
    return steer;    
  }

/**
*Lien Flock
*/
  PVector cohesion (ArrayList<Mover> boids) {
    if (sumCohesion == null) {
      sumCohesion = new PVector();      
    } else {
      sumCohesion.mult(0);
    }

    int count = 0;

    for (Mover other : boids) {
      float d = PVector.dist(location, other.location);

      if (d > 0 && d < radiusCohesion) {
        sumCohesion.add(other.location);
        count++;
      }
    }

    if (count > 0) {
      sumCohesion.div(count);

      return seek(sumCohesion);
    } else {
      return zeroVector;
    }
    
  }

/**
*Application des Force
*/ 
  void applyForce (PVector force) {
    PVector f;
    
    if (mass != 1)
      f = PVector.div (force, mass);
    else
      f = force;
   
    this.acceleration.add(f);    
  }
  
/**
*Affichage du Debug
*/  
  void renderDebug() {
    pushMatrix();
      noFill();
      translate(location.x, location.y);
      
      strokeWeight(1);
      stroke (100, 0, 0);
      ellipse (0, 0, radiusSeparation, radiusSeparation);

      stroke (0, 100, 0);
      ellipse (0, 0, radiusAlignment, radiusAlignment);

      stroke (0, 0, 200);
      ellipse (0, 0, radiusCohesion, radiusCohesion);
      
    popMatrix();

    if (msgCount % 60 == 0) {
      msgCount = 0;

      if (debugMessage != "") {
        println(debugMessage);
        debugMessage = "";
      }
    }

    msgCount++;
  }
/**
*Separation Image
*/  
  void direction()
  {
    face = ennemi.get(0,hFace*dirFace,wEnnemi,hFace);
  }
/**
*Choix Image
*/
  void displayImage(int etat)
  {
    
    if(abs(velocity.x) > abs(velocity.y))
    {
      if(velocity.x > 0)
      {
        dirFace = 2;
      }
      else
      {
        dirFace = 1;     
      }
    }
    else
    {
      if(velocity.y > 0)
      {
        dirFace = 0;     
      }
      else
      {
        dirFace = 3;        
      }
    }
    direction();
    
    
    image(face.get(wFace*etat,0,wFace,hFace),0,0);
  }
}

/**
*source Nicolas Bourré
*/
